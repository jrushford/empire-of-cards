export const checkIfSelected = (card, selectedCards) =>
  Boolean(selectedCards.find((item) => item.title === card.title));
